﻿using Pablo.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pablo.ViewModels
{
    public class ReporteVM
    {
        public List<PromedioDTO> promedio { get; set; }
    }
}