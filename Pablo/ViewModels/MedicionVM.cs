﻿using Pablo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pablo.ViewModels
{
    public class MedicionVM
    {
        public Medicion medicionModel { get; set; }
        public List<Estacion> estacion { get; set; }
    }
}