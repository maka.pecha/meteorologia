﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pablo.DataTransferObjects
{
    public class PromedioDTO
    {
        public string estacion { get; set; }
        public float promedioTemperatura { get; set; }
        public float promedioHumedad { get; set; }
    }
}